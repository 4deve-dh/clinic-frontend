import { Component, OnInit } from '@angular/core';
import { Globals } from '../../services/globals';
import { ApiService } from '../../services/api/api.service';

@Component({
	selector: 'app-notification-page',
	templateUrl: './notification-page.component.html',
	styleUrls: ['./notification-page.component.scss']
})
export class NotificationPageComponent implements OnInit {
	notifications: any
	
	constructor(public globals: Globals, private api: ApiService) { }

	ngOnInit() {
		this.api.read(null, null, this.api.endPoints.notifications + this.globals.currentUser.branch).then(res => {
			this.notifications = res
		})
	}

}
